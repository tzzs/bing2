path="/webdata/bing2";
backup_path="/webdata/backup/bing2";
log="/webdata/log/bing2_backup.log";

#执行结果输出
exec 1>>$log
exec 2>>$log

mkdir -p "/webdata/log";
mkdir -p "${backup_path}"
mkdir -p "${backup_path}/images"
mkdir -p "${backup_path}/log"

date "+%Y-%m-%d %H:%M:%S"
cd /webdata/bing2;
#git push origin server;

#备份数据库
cp -p ${path}/db.sqlite3 ${backup_path}
#备份图片
cp -rfp ${path}/bing/static/bing/images ${backup_path}
#备份日志n
cp -rfp ${path}/log/ ${backup_path}

echo -e "备份完成\n"