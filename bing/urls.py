from django.urls import path, re_path
from . import views

app_name = 'bing'
urlpatterns = [
    path('index2/', views.index, name='index'),  # 无模板
    path('index/', views.index2, name='index2'),  # 无模板
    path('', views.index2, name="index2"),  # 目前主页
    path('today/', views.today, name='today'),
    path('day/<int:day_id>/', views.day, name='day'),
    path('about/', views.about, name='about'),
    path('blog/', views.blog, name='blog'),
    path('blogdetail/', views.blogdetail, name='blogdetail'),
    path('contact', views.contact, name='contact'),
    path('detail', views.detail, name='detail'),
    re_path(r'^index.*/', views.index2, name='index')
]
