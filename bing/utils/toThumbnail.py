from PIL import Image
import os


def get_thumbnail(file_path):
    if file_path.split('.')[-1] == 'jpg' and file_path.split('.')[-2] != 'jpg':
        img = Image.open(file_path)
        w, h = img.size
        img.thumbnail(((int)(w / 4), (int)(h / 4)))
        img.save(file_path + '.jpg')
        return "缩略图生成完成..."
    else:
        return "缩略图已存在..."


if __name__ == '__main__':
    server_images_path = '/webdata/bing2/bing/static/bing/images'  # 服务器图片存储地址
    local_path = '../static/bing/images'
    for s in os.listdir(server_images_path):
        # ss = s.split('.')
        # if ss[-1] != ss[-2]:
        #     # print(s)
        print(get_thumbnail(server_images_path + '/' + s))
