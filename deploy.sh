path="/webdata/bing2";
backup_path="/webdata/backup/bing2";
log="/webdata/log/bing2_deploy.log";

#执行结果输出
exec 1>>$log
exec 2>>$log

date "+%Y-%m-%d %H:%M:%S"

#安装PIL
pip3 install --upgrade pip
pip3 install pillow

cd ${path}

#运行备份脚本
#./backup.sh

#信息上传
git checkout server
git add .
git commit -m "server commit"
git push origin server

#获取更新
git checkout master
git pull origin master

#更新图片和数据库
cp -rfp ${backup_path}/images ${path}/bing/static/bing/
cp -rfp ${backup_path}/db.sqlite3 ${path}

#复制nginx配置文件
cp /webdata/bing2/bing2.conf /etc/nginx/conf.d/

#生成缩略图
#python3 ${path}/bing/utils/toThumbnail.py

#重启uwsgi
uwsgi3 --ini ${path}/uwsgi.ini

#重启nginx
#nginx -s reload
#systemctl restart nginx